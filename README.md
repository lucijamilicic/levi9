# Levi9 Calendar   

![](https://img.shields.io/badge/React-frontend-blue?logo=react&style=for-the-badge)
![](https://img.shields.io/badge/Node.js-backend-green?logo=node.js&style=for-the-badge)
![](https://img.shields.io/badge/MongoDB-database-brightgreen?logo=mongodb&style=for-the-badge)

**Project is made for educational purposes.**


# Description

**Meeting scheduling web app.**

1. Double click on date to add new meeting
2. Click on meeting title to show details
3. Click on 'And x more' to show all meetings scheduled for that date 

# Instructions

1. clone repository:
``` 
git clone https://gitlab.com/lucijamilicic/levi9.git
```

2. start mongodb:
``` 
sudo systemctl start mongod
```

3. from directory **data** import database content
``` 
./import.sh
```

4. from directory **server** start server app:
``` 
node server.js
```

5. from directory **calendar** start app:
``` 
npm run dev
```

6. app is active on https://localhost:3000
