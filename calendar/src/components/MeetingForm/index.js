import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Multiselect from "multiselect-react-dropdown";
import styles from "./meetingform.module.css";

const MeetingForm = ({ closeModal, date }) => {
  const [allParticipants, setAllParticipants] = useState([]);
  const router = useRouter();

  useEffect(() => {
    fetch("http://localhost:5000/api/users")
      .then((res) => res.json())
      .then((data) => setAllParticipants(data));
  }, []);

  const [title, setTitle] = useState("");
  const [time, setTime] = useState("");
  const [description, setDescription] = useState("");
  const [participants, setParticipants] = useState([]);

  const multiselectList = useRef();

  const resetForm = (e) => {
    e.preventDefault();
    setTitle("");
    setTime("");
    setDescription("");
    setParticipants([]);
    multiselectList.current.resetSelectedValues();
    closeModal();
  };

  const submitForm = (e) => {
    e.preventDefault();

    if (validateForm()) {
      const newMeeting = {
        title: title,
        time: time,
        description: description,
        participants: participants.map((x) => x.firstName + " " + x.lastName),
        day: date,
      };
      sendRequest(newMeeting);
      resetForm(e);
    } else {
      window.alert("Please enter all information");
    }
  };

  const sendRequest = (newMeeting) => {
    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newMeeting),
    };

    fetch("http://localhost:5000/api/meetings", options)
      .then((res) => res.json())
      .then((data) => router.push("/meeting/" + data._id));
  };

  const validateForm = () => {
    let valid = true;
    if (title.length <= 0) valid = false;
    if (time.length <= 0) valid = false;
    if (description.length <= 0) valid = false;

    return valid;
  };

  return (
    <form onSubmit={(e) => submitForm(e)} className={styles.form}>
      <h2>Schedule new meeting for January/{date}/2022 </h2>
      <div className={styles.group}>
        <label className={styles.label}>Title: </label>
        <input
          type="text"
          placeholder="Enter title..."
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          className={styles.input}
        />
      </div>
      <div className={styles.group}>
        <label className={styles.label}>Time: </label>
        <input
          type="text"
          placeholder="Enter time..."
          value={time}
          onChange={(e) => setTime(e.target.value)}
          className={styles.input}
        />
      </div>
      <div className={styles.group}>
        <label className={styles.label}>Description: </label>
        <textarea
          type="text"
          placeholder="Short description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className={styles.input}
        />
      </div>
      <div className={styles.group}>
        <label>Add participants: </label>
        <Multiselect
          options={allParticipants}
          selectedValues={[]}
          onSelect={(selectedList, selectedItem) =>
            setParticipants(selectedList)
          }
          onRemove={(selectedList, removedItem) =>
            setParticipants(selectedList)
          }
          displayValue="firstName"
          ref={multiselectList}
        />
      </div>
      <button type="submit" className={styles.button}>
        Save
      </button>
      <button onClick={(e) => resetForm(e)} className={styles.button}>
        Close
      </button>
    </form>
  );
};

export default MeetingForm;
