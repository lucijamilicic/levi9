import styles from "./modal.module.css";

const Modal = ({ show, children }) => {
  const showHideClassName = show ? styles.display : styles.hide;

  return (
    <div className={showHideClassName}>
      <section className={styles.main}>{children}</section>
    </div>
  );
};

export default Modal;
