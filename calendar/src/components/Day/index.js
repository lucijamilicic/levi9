import { useRouter } from "next/router";
import styles from "./day.module.css"

const Day = ({ day, meetings, openModal }) => {
  if (day == "") return <></>;

  const router = useRouter();

  return (
    <div className={styles.day} onDoubleClick={() => openModal()}>
      <h1>{day}</h1>
      <p
        className={styles.event}
        onClick={() => router.push("/meeting/" + meetings[0]?._id)}
      >
        {meetings[0]?.title}
      </p>
      <p className={styles.time}>{meetings[0]?.time}</p>
      <a onClick={() => router.push("/jan/" + day)}>
        {meetings.length > 1 ? "And " + (meetings.length - 1) + " more" : ""}
      </a>
    </div>
  );
};

export default Day;
