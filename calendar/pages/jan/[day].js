import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import styles from "./dailymeetings.module.css";

const DailyMeetings = () => {
  const router = useRouter();
  const { day } = router.query;
  const [meetings, setMeetings] = useState([]);

  useEffect(() => {
    fetch("http://localhost:5000/api/meetings/day/" + day)
      .then((res) => res.json())
      .then((data) => setMeetings(data));
  }, [day]);

  return (
    <div className={styles.details}>
      <h1>All meetings for: January/{day}/2022</h1>
      {meetings.length ? (
        meetings?.map((meeting, index) => (
          <div key={index} className={styles.meeting}>
            <p onClick={() => router.push("/meeting/" + meeting._id)}>
              {meeting.time} - {meeting.title}
            </p>
          </div>
        ))
      ) : (
        <h2>Free time!</h2>
      )}
    </div>
  );
};

export default DailyMeetings;
