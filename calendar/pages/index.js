import Day from "../src/components/Day";
import { useState, useEffect } from "react";
import Modal from "../src/components/Modal";
import MeetingForm from "../src/components/MeetingForm";
import styles from "../styles/Home.module.css";

export default function Home() {
  // const month = [
  //   ["01", "02", "03", "04", "05", "06", "07"],
  //   ["08", "09", "10", "11", "12", "13", "14"],
  //   ["15", "16", "17", "18", "19", "20", "21"],
  // ];

  const month = [
    ["", "", "", "", "", "01", "02"],
    ["03", "04", "05", "06", "07", "08", "09"],
    ["10", "11", "12", "13", "14", "15", "16"],
    ["17", "18", "19", "20", "21", "22", "23"],
    ["24", "25", "26", "27", "28", "20", "30"],
    ["31"],
  ];

  const [meetings, setMeetings] = useState([]);
  const [newMeetingDate, setDate] = useState([]);

  useEffect(() => {
    fetch("http://localhost:5000/api/meetings")
      .then((res) => res.json())
      .then((data) => setMeetings(data));
  }, []);

  const [modalShow, toggleModal] = useState(false);

  return (
    <div className={styles.main}>
      <h1>January 2022.</h1>
      <table>
        <thead>
          <tr>
            <td className={styles.dayname}>MON</td>
            <td className={styles.dayname}>TUE</td>
            <td className={styles.dayname}>WED</td>
            <td className={styles.dayname}>THU</td>
            <td className={styles.dayname}>FRI</td>
            <td className={styles.dayname}>SAT</td>
            <td className={styles.dayname}>SUN</td>
          </tr>
        </thead>
        <tbody>
          {month.map((week, index) => (
            <tr key={index}>
              {week.map((day, index) => (
                <td key={index}>
                  <Day
                    day={day}
                    meetings={meetings.filter(
                      (x) => Number(x.day) == Number(day)
                    )}
                    openModal={() => {
                      toggleModal(true);
                      setDate(day);
                    }}
                  ></Day>
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      <Modal show={modalShow}>
        <MeetingForm
          closeModal={() => toggleModal(false)}
          date={newMeetingDate}
        ></MeetingForm>
      </Modal>
    </div>
  );
}
