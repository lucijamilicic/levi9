const express = require('express');
const meetingsControler = require('../../contollers/meetings');

const router = express.Router();

router.get('/', meetingsControler.getAllMeetings);
router.post('/', meetingsControler.addNewMeeting);

router.get('/:id', meetingsControler.getMeetingById);
router.post('/:id', meetingsControler.updateMeeting);
router.delete('/:id', meetingsControler.deleteMeeting);

router.get('/day/:day', meetingsControler.getMeetingsByDay);


module.exports = router;
