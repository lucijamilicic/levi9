const express = require('express');
const usersController = require('../../contollers/users');

const router = express.Router();
router.get('/', usersController.getAllUsers);

module.exports = router;
