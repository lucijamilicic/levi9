const mongoose = require("mongoose");

const meetingSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  day: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  time: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  description: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  participants: {
    type: [mongoose.Schema.Types.String],
    required: true,
  },
});

const Meeting = mongoose.model("Meeting", meetingSchema);

module.exports = Meeting;
